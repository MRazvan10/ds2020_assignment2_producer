import org.json.simple.JSONObject;

public class Main {
    public static void main(String[] args) {
        Producer producer = new Producer();
        ActivityReader activityReader = new ActivityReader();

        for (Activity activity : activityReader.getActivitati()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("pacientId", activity.getPacientId());
            jsonObject.put("activitate", activity.getActivity());
            jsonObject.put("startTime", activity.getStartTime());
            jsonObject.put("endTime", activity.getEndTime());
            try {
                producer.send(jsonObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}