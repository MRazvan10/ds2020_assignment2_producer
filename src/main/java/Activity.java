import java.io.Serializable;

public class Activity implements Serializable {
    private final Integer pacientId;
    private final String activity;
    private final long startTime, endTime;

    public Activity(Integer pacientId, String activity, long startTime, long endTime) {
        this.pacientId = pacientId;
        this.activity = activity;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Integer getPacientId() {
        return pacientId;
    }

    public String getActivity() {
        return activity;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }
}
