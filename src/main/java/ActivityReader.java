import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class ActivityReader {
    public List<Activity> getActivitati() {
        List<Activity> activitati = new ArrayList<>();
        try {
            FileReader fileReader = new FileReader("D:\\facultate\\sd\\tema2\\activity.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String l;

            while ((l = bufferedReader.readLine()) != null) {
                StringTokenizer stringTokenizer = new StringTokenizer(l);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                long startDate = simpleDateFormat.parse(stringTokenizer.nextToken() + " " + stringTokenizer.nextToken()).getTime();
                long endDate = simpleDateFormat.parse(stringTokenizer.nextToken() + " " + stringTokenizer.nextToken()).getTime();
                String act = stringTokenizer.nextToken();

                Activity activity = new Activity(1, act, startDate, endDate);
                activitati.add(activity);
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return activitati;
    }
}