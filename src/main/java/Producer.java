import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.json.simple.JSONObject;

public class Producer {

    public void send(JSONObject jsonObject) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare("Tema2", false, false, false, null);
        channel.basicPublish("", "Tema2", null, jsonObject.toJSONString().getBytes());
        System.out.println("Sent : " + jsonObject.toJSONString());
    }
}